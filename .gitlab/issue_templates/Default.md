# Event Task

Issues can have broad or narrow focus.

Which staff member is responsible for this task?

- [ ] Checklist Item 1 
- [ ] Checklist Item 2
- [x] Checklist Item 3 (completed)

Add appropriate label(s).

Select an appropriate milestone. Assigned milestone can be changed as needed.

If this task as a hard due date, enter in the Due Date field.
